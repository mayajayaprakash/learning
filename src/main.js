import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import api from './components/api.vue';
import HelloWorld from './components/HelloWorld.vue';
import Welcome from './components/Welcome.vue';
// eslint-disable-next-line no-unused-vars
import axios from 'axios';
window.axios = require('axios');
Vue.config.productionTip = false;

Vue.use(VueRouter);
window.EventBus = new Vue()



const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', name: 'Welcome', component: Welcome },
    //{ path: '/Welcome', name: 'Welcome', component: Welcome },
    { path: '/api', name: 'api', component: api },
    { path: '/HelloWorld', name: 'HelloWorld', component: HelloWorld },
    { path: '*', redirect: '/Welcome' }
  ]
});

new Vue({
  methods: {
    grtapi: function() {
      alert(this.grtapi);
    }
  },
  router,
  render: h => h(App)
}).$mount('#app');
